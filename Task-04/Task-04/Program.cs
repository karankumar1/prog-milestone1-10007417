﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04
{
    class Program
    {
        static void Main(string[] args)
        {
            double fahrenheit;
            double celsius = 0;
            int r;
            Console.WriteLine("press 1 for change Celsius to Fahrenheit or any other number for change Fahrenheit to Celsius ");
            r = Convert.ToInt16(Console.ReadLine());
            if (r == 1)
            {
                Console.WriteLine("Enter the temperature to convert from  Celsius to Fahrenheit");
                celsius = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("The entered celsius temperature is: " + celsius);
                fahrenheit = (celsius * 5 / 9) + 32;
                Console.WriteLine("The converted temperature is " + fahrenheit + " degrees Celsius");
            }
            else
            {
                Console.WriteLine("Enter the temperature to convert from Fahrenheit to Celsius");
                fahrenheit = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("The entered Farenheit temperature is: " + fahrenheit);
                celsius = (fahrenheit - 32) * 5 / 9;
                Console.WriteLine("The converted temperature is" + celsius + " degrees Celsius");
            }
            Console.ReadKey();
        }
    }
}
