﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_27
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> list = new List<string>();
            list.Add("Red");
            list.Add("Blue");
            list.Add("Yellow");
            list.Add("Green");
            list.Add("Pink");


            list.Reverse();

            foreach (string value in list)
            {
                Console.WriteLine(value);


            }
            Console.ReadKey();
        }
    }
}
