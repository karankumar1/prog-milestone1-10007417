﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01
{
    class Program
    {
        static void Main(string[] args)
        {
            var age = 0;
            string name;
            Console.WriteLine("What is your Name?");
            name = Console.ReadLine();
            Console.WriteLine("What is your age?");
            age = int.Parse(Console.ReadLine());
            Console.Write("Your name is {0} and age is {1} ", name, age);
            Console.Write("\n Your name is " + name + "and age is " + age);
            Console.WriteLine($"\n Your name is {name} and age is {age}");
            Console.ReadKey();
        }
    }
}
