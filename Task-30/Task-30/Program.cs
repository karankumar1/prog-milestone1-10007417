﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_30
{
    class Program
    {
        static void Main(string[] args)
        {
            var month = "";
            var day = "";

            Console.WriteLine(" Write the  month you were born in");
            month = Console.ReadLine();
            Console.WriteLine(" Enter the  number of the day you were born");
            day = Console.ReadLine();

            Console.WriteLine($"You were born on  {month} the {day}th");
        }
    }
}
