﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_20
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> even = new List<int>(new int[] {33,45,21,44,67,87,86});

           
            foreach (int number in even)
            {
                if (number%2 != 0) 
                {
                    Console.WriteLine("{0} is odd number",number);
                }
            }
            Console.ReadKey();
        }
    }
}
