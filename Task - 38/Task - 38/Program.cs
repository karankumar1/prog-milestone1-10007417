﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task___38
{
    class Program
    {
        static void Main(string[] args)
        {
            int n, i;
            Console.WriteLine(" multiplication table: ");
            n = Int32.Parse(Console.ReadLine());
            for (i = 1; i <= 12; ++i)
                Console.WriteLine("{0}*{1}={2}", i,n ,i * n);
        }
    }
}
