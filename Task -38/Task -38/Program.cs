﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task__38
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> list = new List<string>();
            list.Add("Ravi, March, 20 March");
            list.Add("Sanket, April, 30 November");
            list.Add("Vikram, March, 04 December");


            // Reverse List in-place, no new variables required.
            list.Reverse();

            foreach (string value in list)
            {
                Console.WriteLine(value);

            }
            Console.ReadKey();
        }
    }
}
